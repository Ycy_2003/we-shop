// components/com-list/com-list.js
Component({
  properties: {
    list:{
      type:Array,
      value:[]
    }
  },
  methods:{
    goDetail(e){
      let item_id = e.currentTarget.dataset.item.id
      wx.navigateTo({
        url: '/pages/detail/detail?id='+item_id,
      })
    }
  }
})
