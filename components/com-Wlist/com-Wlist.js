// components/com-Wlist/com-Wlist.js
Component({
  properties: {
    list:{
      type:Array,
      value:[]
    },
    pd:{
      type:Boolean,
      value:false
    },
    goodid:{
      type:Boolean,
      value:false
    }
  },
  methods:{
    goDetail(e){
      let item = e.currentTarget.dataset.item
      if(this.data.pd){
        var item_id = item.value_id
      }else{
        var item_id = item.id
      } 
      if(this.data.goodid){
        var item_id = item.goods_id
      }
      wx.navigateTo({
        url: '/pages/detail/detail?id='+item_id,
      })
    }
  }
})
