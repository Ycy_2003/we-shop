// components/com-address/com-address.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    list:Array,
    delPD:{
      type:Boolean,
      value:true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    del(e){
      let item = e.currentTarget.dataset.item
      this.triggerEvent('son',{item})
    },
    goDetail(e){
      let item = e.currentTarget.dataset.item
      this.triggerEvent('detail',{item})
    }
  }
})
