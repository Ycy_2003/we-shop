// pages/user/user.js
import api from '../../methods/api.js'
import _ from 'underscore'
Page({
  data:{
    user:null || wx.getStorageSync('user'),
    my_list:[
      {name:'我的订单',image:"-437.5rpx",path:'/order/order'},
      {name:'优惠券',image:"-62.4997rpx",path:'/coupons/coupons'},
      {name:'礼品卡',image:"-187.5rpx",path:'/TheGiftCard/TheGiftCard'},
      {name:'我的收藏',image:"0",path:'/collect/collect'},
      {name:'我的足迹',image:'-500rpx',path:'/footprint/footprint'},
      {name:'会员福利',image:'-312.5rpx',path:'/vip/vip'},
      {name:'地址管理',image:'0',path:'/address/address'},
      {name:'账号安全',image:'-500rpx',path:'/security/security'},
      {name:'联系客服',image:'-312.5rpx',path:'/CustomerService/CustomerService'},
      {name:'帮助中心',image:'-250rpx',path:'/help/help'},
      {name:'意见反馈',image:'-125rpx',path:'/feedback/feedback'}
    ],
    show:false,
    num:0
  },
  login(){
    if(this.data.user){
      this.unlogin()
      return false
    }
    wx.navigateTo({
      url: '/pages/login/login',
    });
  },
  onShow(){
    this.setData({
      user:wx.getStorageSync('user')
    })
  },
  unlogin(){   //退出登录方法
    let this_ = this
    wx.removeStorage({
      key: 'user',
      success(res){
        this_.setData({user:null})
      }
    })
  },
  bindtapList(e){
    let item_path = e.currentTarget.dataset.item.path
    wx.navigateTo({
      url: '/pages/userList'+item_path,
    })
  },
})