// pages/featureDetailPings/featureDetailPings.js
import api from '../../methods/api.js'
Page({
  data:{
    id:null,
    page:1,
    size:10,
    list:[],
    isXia:false
  },
  onLoad(options){
    this.setData({
      id:options.id
    })
    this.getList()
  },
  async getList(pd = false){
    let res = await api.getFetures.PingList(this.data.id,1,this.data.size,this.data.page)
    if(pd) this.setData({list:[],isXia:false})   //控制 下拉刷新请求成功后 让起数组先清空,下拉状态设为false
    let str = `list[${this.data.list.length}]`
    this.setData({
      [str]:res.data.data.data,
      count:res.data.data.count
    })
  },
  scrollXia(){  //下拉刷新
    this.setData({page:1})
    this.getList(true)
  },
  scrollBottom(){
    let countPD = this.data.size * this.data.page > this.data.count ? true : false
    if(countPD) return false
    this.setData({
      page:this.data.page+1
    })
    this.getList()
  }

})