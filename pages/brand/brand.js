import api from '../../methods/api.js'
Page({
  data:{
    brand:[],
    list:[]
  },
  onLoad(option){
    let id = option.id
    api.getBrand(id).then(res=>{
      this.setData({
        brand:res.data.data.brand
      })
    })
    api.getBrandLis(id,1,1000).then(res=>{
      this.setData({
        list:res.data.data.goodsList
      })
    })
      
  }
})