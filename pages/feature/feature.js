// pages/feature/feature.js
import api from '../../methods/api.js'
Page({
  data:{
    list:[],
    page:0,
    size:10,
    isTop:false,
    scrollTop:'0px',
    isXia:false
  },
  onLoad(){
    this.getList()
  },
  xia(){
    this.setData({
      page:0,
    })
    this.getList(true)
  },
  async getList(pd=false){
    let res = await api.getFeture(this.data.page,this.data.size)
    let str = `list[${this.data.list.length}]`
    if(res.data.data.count<this.data.page * this.data.size){
      wx.showToast({
        title:'没有更多内容',
        icon:'none'
      })
      return false
    }
    if(pd) {
      this.setData({
        list:[],
      })
    }
    this.setData({
      [str]:res.data.data.data,isXia:false
    })
  },
  scrollBottom(){
    this.setData({
      page:this.data.page+1
    })
    this.getList()
  },
  scroll(e){
    let scrolltop = e.detail.scrollTop
    let pd = false
    if(scrolltop>1000){
      if(this.data.isTop) return
      pd = true      
    }else{
      if(!this.data.isTop) return
      pd = false
    }
    this.setData({
      isTop:pd
    })
  },
  goZT(e){     //去专题详情
    let item = e.currentTarget.dataset.item
    console.log(item);
    wx.navigateTo({
      url: '/pages/featureDetail/featureDetail?id='+item.id,
    })
  },
  goTop(){
   this.setData({
     scrollTop:'0px'
   }) 
  }
})