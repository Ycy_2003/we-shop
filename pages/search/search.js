import api from '../../methods/api.js'
Page({
  data:{
    val:'',
    isLS:true,
    JL:[],
    LSli:[],
    list:[],
    listShow:false,
    keyword:'',
    show:false,
    paiActive:0,
    paiSwitchActive:null
  },
  heandleInput(){
    this.getSelects()
  },
  getSelects(){
    if(this.data.val==''){
      this.getLsList()
      this.setData({
        isLS:true
      })
      return false
    }
    let i = this.data.val
    api.getLSli(i).then(res=>{
      this.setData({
        isLS:false,LSli:res.data.data,
      })
    })
  },
  inputFocus(){
    this.setData({
      list:[],
      show:false,
    })
    this.getSelects()
  },
  handleList(e){
    let keyword = e.currentTarget.dataset.item
    this.setData({
      val:keyword
    })
    this.getList()
  },
  search(){ //回车事件
    let keyword  = this.data.val
    this.setData({
      list:[],val:keyword,LSli:[]
    })
    this.getList('id','desc')
    this.getLsList()
  },
  async getList(sort,order){  //获取查询后的列表
    if(this.data.val=='') return false
    let res = await api.getSou(this.data.val,this.data.page,this.data.size,sort,order)
    let str = `list[${this.data.list.length}]`
    if(res.data.data.data.length == 0){
      this.setData({
        listShow:true
      })
      return false
    }
    this.setData({
      [str]:res.data.data.data,
      show:true,listShow:false,
      LSli:[]
    })
  },
  handleTag(e){  //历史记录 单击
    let item = e.currentTarget.dataset.item
    this.setData({
      val:item,
      isLS:false,
      list:[]
    })
    this.getList('id','desc')
  },

  back(){   //取消
    wx.navigateBack()
  },
  async del(){  //删除记录
    await api.delLSList()
    this.getLsList()
  },
  cha(){
    this.setData({
      val:'',
      LSli:[],
      show:false,
      list:[],
      isLS:true,
      listShow:false
    })
    this.getLsList()
  },

  onShow(){
    this.getLsList() 
  },
  async getLsList(){   //记录获取
    let res = await api.getLsList()
    this.setData({
      JL:res.data.data
    })
  },

  pai_zong(){  //综合排行
    this.setData({
      list:[],
      paiActive:0,
      paiSwitchActive:null
    })
    this.getList(undefined,'desc',true)
  },
  paiSwitch(){
    let i = null
    if(this.data.paiSwitchActive == 0) i = 1
    else i = 0
    if(!this.data.paiSwitchActive && this.data.paiSwitchActive!==0) i =0
    this.setData({
      paiSwitchActive:i,
      paiActive:1,
      list:[]
    })
    if(i == 0) this.getList('price','asc') 
    else this.getList('price','desc')
  }
})