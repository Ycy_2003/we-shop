import api from '../../methods/api.js'
Page({
  data:{
    home:{},
    isTop:false,
    scrollTop:'0px',
    isXia:false
  },
  handleXia(){
    this.getHome()
  },
  handleScroll(e){
    let scrollTop = e.detail.scrollTop
    let pd = false
    if(scrollTop>1000){
      pd = true
      if(this.data.isTop) return false
    }else{
      pd=false
      if(!this.data.isTop) return false
    } 
    this.setData({
      isTop:pd
    })
  },
  goTop(){
    this.setData({
      scrollTop:'0px'
    })
  },
  smallClassTap(e){  //小分类
    let item = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: item.url,
    })
  },
  async onLoad(){
    this.getHome()
  },
  async getHome(){
    let res = await api.getHome()
    this.setData({
      home:res.data.data,isXia:false
    })
  },
  // 专题精选 tap 去feturedetail
  goFeatureDetail(e){
    let id = e.currentTarget.dataset.item.id
    wx.navigateTo({
      url: '/pages/featureDetail/featureDetail?id='+id,
    })
  },

  goBrand(e){
    let item_id = e.currentTarget.dataset.item.id
    wx.navigateTo({
      url: '/pages/brand/brand?id='+item_id,
    })
  },

  goDetail(e){
    let item_id = e.currentTarget.dataset.item.id
    wx.navigateTo({
      url: '/pages/detail/detail?id='+item_id,
    })
  }
})