// pages/category/category.js
import api from '../../methods/api.js'
Page({
  data:{
    category:[],
    active:0,
    scrollTop:'0px',
    isXia:false,
    isTop:false,
    page:0,
    size:100,
    list:[],
    x:0
  },
  handleXia(){   //下拉刷新
    this.getList(true)
  },
  handleScroll(e){   //移动事件
    let scrollTop = e.detail.scrollTop
    let pd = false
    if(scrollTop>700){
      pd = true
      if(this.data.isTop) return false
    }else{
      pd=false
      if(!this.data.isTop) return false
    } 
    this.setData({
      isTop:pd
    })
  },
  goTop(){
    this.setData({
      scrollTop:'0px'
    })
  },
  scrollDi(){
    this.setData({
      page:this.data.page+1
    })
    this.getList()
  },
  tabTap(e){
    let id = e.currentTarget.dataset.item.id
    this.setData({id,page:0})
    this.getCateGory(true)
  },
  async onLoad(option){
    let id = option.id
    this.setData({id})
    this.getCateGory()
  },
  async getCateGory(pd=false){    //根据 id  进入时获取的
    let res = await api.getCategory(this.data.id)
    res.data.data.brotherCategory.map((v,k)=>{   //计算默认显示的 样式 key
      if(v.name == res.data.data.currentCategory.name){
        this.setData({
          active:k,
        })
      }
    })
    this.setData({  //设置
      category:res.data.data,
      x:100*this.data.active+'rpx'   //用于上方 x 轴移动
    })
    this.getList(pd)
  },
  async getList(pd=false){
    let list = await api.getCategoryList(this.data.id,this.data.page,this.data.size)
    if(pd){
      this.setData({list:[],page:0})
    }
    let str = `list[${this.data.list.length}]`
    if(list.data.data.data.length == 0){
      wx.showToast({
        title:'没有更多内容',
        icon:'none'
      })
      return false
    }
    this.setData({
      [str]:list.data.data.data,
      isXia:false
    })
  },
  goDetail(e){
    let item_id = e.currentTarget.dataset.item.id
    wx.navigateTo({
      url: '/pages/detail/detail?id='+item_id,
    })
  }
})