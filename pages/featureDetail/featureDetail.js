// pages/featureDetail/featureDetail.js
import api from '../../methods/api.js'
import WxParse from '../../methods/wxParse/wxParse.js'
Page({
  data:{
    tlist:[],
    isTop:false,
    scrollY:null
  },
  async onLoad(option){
    let id = option.id
    this.setData({ID:id})

    let detail = await api.getFetures.Detail(id)  //获取详情内容
    // 使用 html/md 解析
    let htmlstr = detail.data.data.content
    let that = this 
    WxParse.wxParse('htmlstr', 'md', htmlstr, that, 0);
    this.getRelated()
  },
  async onShow(){   //获取评论列表
    let id = this.data.ID
    let s = await api.getFetures.PingList(id,1,5,1)
    this.setData({
      pingS:s.data.data.data
    })
  },
  async getRelated(){
    let id = this.data.ID
    let related = await api.getFetures.related(id)
    console.log(related.data.data);
    this.setData({
      tlist:related.data.data
    })
  },
  goZT(e){    
    let id = e.currentTarget.dataset.item.id
    wx.navigateTo({
      url: '/pages/featureDetail/featureDetail?id='+id,
    })
  },
  goPingList(){   //去评论列表
    wx.navigateTo({
      url:'/pages/featureDetailComments/featureDetailComments?id='+this.data.ID
    })
  },
  scroll(e){   //滚动事件
    let scrollTop = e.detail.scrollTop
    let pd = false
    if(scrollTop>1000){
      if(this.data.isTop) return false
      pd = true
    }else{
      if(!this.data.isTop) return false
      pd = false
    }
    this.setData({
      isTop:pd
    })
  },
  goTop(){  //回到顶部
    this.setData({
      scrollY:'0px'
    })
  },
  goAddComment(){  //去添加评论页
    wx.navigateTo({
      url: '/pages/fetureAddComment/fetureAddComment?id='+this.data.ID,
    });
  }
})