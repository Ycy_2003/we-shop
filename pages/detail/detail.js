// pages/detail/detail.js
import api from '../../methods/api.js'
import WxParse from '../../methods/wxParse/wxParse.js'

Page({
  data:{
    detail:[],
    show:false,
    isTop:false,
    isXia:false,
    scrollY:'0px',
    lis:[],
 
    isAddShopping:true,
    addShoppingNum:1,

    shoppingCount:''
  },
  scrollXia(){ //下拉刷新
    this.getDetail()
    this.getShoppingCount()
  },
  scroll(e){  //滚动事件
    let scrollTop = e.detail.scrollTop
    let pd = false
    if(scrollTop > 700){
      if(this.data.isTop) return
      pd = true
    }else{
      if(!this.data.isTop) return
      pd = false
    }
    this.setData({
      isTop:pd
    })
  },
  goTop(){
    this.setData({
      scrollY:'0px'
    })
  },
  onLoad(option){
    let id = option.id
    this.setData({id})
  },
  onShow(){
    this.getDetail()
    this.getShoppingCount()
  },
  getDetail(){  //获取详情信息
    api.getDetail(this.data.id).then(res=>{ 
      this.setData({
        detail:res.data.data,
        isXia:false
      })
      let detailHTML = res.data.data.info.goods_desc
      let that = this;
      WxParse.wxParse('detailHTML', 'html', detailHTML, that, 0);
    })
    api.getDetailGoods(this.data.id).then(res=>{
      this.setData({
        lis:res.data.data.goodsList
      })
    })
  },
  goDetailComments(){  //去评论
    wx.navigateTo({
      url: '/pages/detailComments/detailComments?id='+this.data.id,
    })
  },
  showPopup() {  //显示 弹出层
    this.setData({ show: true ,isAddShopping:false});
  },

  onClose() {  //关闭弹出层
    this.setData({ show: false,isAddShopping:true});
  },

  // 获取购物车 条数
  getShoppingCount(){
    api.shoppingNum().then(res=>{
      let count = res.data.data.cartTotal.goodsCount
      this.setData({
        shoppingCount:count
      })
    })
  },

  // j加入购物车
  addShopping(){
    if(this.data.isAddShopping){
      this.setData({show:true,isAddShopping:false})
    }else{
      let obj = this.data.detail.productList[0]
      let num = this.data.addShoppingNum
      api.addShopping(obj.goods_id,num,obj.id).then(res=>{
        this.setData({
          show:false,
          isAddShopping:true
        })
        wx.showToast({title:'加入成功'})
        this.getShoppingCount()
      })
    }
  },
  // 去购物车
  pathGoShopping(){
    wx.switchTab({
      url:"/pages/shopping/shopping"
    })
  },
  addShoppingNum(){  //数量++
    this.setData({
      addShoppingNum:this.data.addShoppingNum+1
    })
  },
  delShoppingNum(){   //数量--
    let addshopNum = this.data.addShoppingNum
    if(addshopNum<=1) return false
    addshopNum-=1
    this.setData({
      addShoppingNum:addshopNum
    })
  },
  // 立即购买
  goBuy(){
    console.log('aa');
  },
  Collect(){   //添加收藏
    api.Collect(this.data.id).then(res=>{
      if(res.data.errno == 401){  //判断是否登录,没有登录去登陆页
        wx.navigateTo({
          url: '/pages/login/login',
        });
        return false 
      }
      this.getDetail()
    })
  }
})