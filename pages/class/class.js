// pages/class/class.js
import api from '../../methods/api.js'
Page({
  data:{
    list:[],
    active:0,
    isXia:false
  },
  scrollXia(){
    this.getRightList()
  },
  async onLoad(){
    await this.getLeftList()
    this.getRightList()
  },
  async getLeftList(){
    let res = await api.getClass()
    let allList = res.data.data.categoryList
    let activeListId = res.data.data.currentCategory.id
    this.setData({
      allList,
      activeListId
    })
  },
  async getRightList(){
    let res = await api.getRightClass(this.data.activeListId)
    this.setData({
      rightList:res.data.data.currentCategory,
      isXia:false
    })
  },
  tabSwitch(e){
    let itemid = e.currentTarget.dataset.item.id
    this.setData({
        activeListId:itemid
    })
    this.getRightList()
  },
  gotFeatureDetail(e){
    let itemid = e.currentTarget.dataset.item.id
    wx.navigateTo({
      url: '/pages/category/category?id='+itemid,
    })
  }
})