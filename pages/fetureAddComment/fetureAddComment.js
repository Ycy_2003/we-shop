// pages/fetureAddComment/fetureAddComment.js
import api from '../../methods/api.js'
Component({
  data:{
    value:''
  },
  methods:{
    black(){
      wx.navigateBack()
    },
    submit(){
      // 判断是否输入内容,没有输入提示
      if(this.data.value.length<=0){ 
        wx.showToast({
          title: '请先输入',
          icon: 'error',
          image: '',
          duration: 1500,
          mask: false,
        });
        return false
      }
      let page = getCurrentPages()
      let parentPage = page[page.length-2]
      let parentID = parentPage.data.ID
      let params = {typeId: 1, valueId: parentID, content:this.data.value}
      api.getFetures.addComment(params).then(res=>{
        if(res.data.errmsg == '请先登录'){  //判断是否登录
          wx.navigateTo({
            url: '/pages/login/login',
          });
          return false
        }
        wx.navigateBack()
      })
    }
  }
})