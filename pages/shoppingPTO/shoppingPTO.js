import api from '../../methods/api.js'
Page({
  data:{
    PTO:{},
  },
  async onShow(){
    let addressID = await wx.getStorageSync('address') || 0
    api.PTO(addressID).then(res=>{
      this.setData({
        PTO:res.data.data
      })
    })
  },
  address(){
    wx.navigateTo({
      url: '/pages/userList/address/address?pd='+true
    })
  },  
  async goFK(){  //去付款
    let addressID = await wx.getStorageSync('address') || 0
    let res1 = await api.PTOafterAddress(addressID)
    if(res1.data.errmsg=='请选择商品'){
      wx.showToast({
        title: '还没选择商品,即将跳转至购物车',
        icon: 'none',
        success: (result) => {
          wx.navigateBack();
        },
      });
      return false 
    }
    if(res1.data.errmsg=='请选择收货地址'){
      console.log('ss');
      wx.showToast({
        title: '请选择收货地址',
        icon: 'none',
      });
      return false
    } 
    let orderId = res1.data.data.orderInfo.id
    let res2 = await api.PTOafterOrder(orderId)
    if(res2.data.errno==400){
      wx.navigateTo({
        url:'/pages/shoppingPTOafter/shoppingPTOafter'
      })
    }
  }
})