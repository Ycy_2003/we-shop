// pages/login/login.js
import api from '../../methods/api.js'
Page({
  user:{},
  async getUserInfo(e){
    let userInfo = e.detail
    let codeObject = await wx.login()
    if(userInfo.errMsg !== 'getUserInfo:ok'){
      wx.showToast({
        title: '登录失败',
        icon: 'none',
      }); 
      return false
    }
    let loginObject = await api.login(codeObject.code,userInfo)
    let user = loginObject.data.data
    wx.setStorage({ 
      key:"user",
      data:user
    })
    wx.navigateBack();
  }
})