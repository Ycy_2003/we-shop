import api from '../../methods/api.js'
Page({
  data:{
    tabs:[
      {tab:'全部',num:0,type:0},
      {tab:'有图',num:0,type:1},
    ],
    active:0,
    page:0,
    list:[],

    scrollY:'0px',
    isXia:false,
    isGoTop:false
  },
  onLoad(options){
    this.setData({id:options.id})
    this.getNum()
    this.getList()
  },
  switchTab(e){   //tab切换
    let activeI = e.currentTarget.dataset.type
    if(activeI == this.data.active) return false
    this.setData({
      active:activeI,
      page:0
    })
    this.getList(true)
  },
  handleScroll(e){  //回到顶部 滚动事件
    let scrollTop = e.detail.scrollTop
    let pd = false
    if(scrollTop>700){
      pd = true
    }else{
      pd = false
    }
    if(this.data.isGoTop == pd) return false
    this.setData({
      isGoTop:pd
    })
  },
  goTop(){ //回到顶部
    this.setData({
      scrollY:'0px'
    })
  },
  scrollXia(){  //下拉刷新
    this.getList(true)
    this.getNum()
  },
  scrollDi(){ //到底部执行
    this.setData({page:this.data.page+1})
    this.getList()
  },
  getNum(){ //获取 信息数量
    api.getDetailCommentNum(this.data.id).then(res=>{
      let str1 = 'tabs[0].num'
      let str2 = 'tabs[1].num'
      this.setData({
        [str1]:res.data.data.hasPicCount,
        [str2]:res.data.data.allCount,
      })
    })
  },
  async getList(pd=false){  //获取列表
    let id = this.data.id;
    let page = this.data.page
    let type = this.data.active
    let res = await api.getDetailComments(id,page,type)
    if(page*10 > res.data.data.count){
      wx.showToast({
        title:"没有更多内容",icon:'none'
      })
      return false
    }
    if(pd){
      this.setData({
        list:[],page:0,isXia:false
      })
    }
    let str = `list[${this.data.list.length}]`
    this.setData({
      [str]:res.data.data.data
    })
  }
})