// pages/userList/addressDetail/addressDetail.js
import api from '../../../methods/api.js'
Page({
  data:{
    detail:{},
    show:false,
    tab:[
      {name:'省份',type:0,getid:1},
      {name:'城市',type:1,getid:0},
      {name:'区县',type:2,getid:0},
    ],
    tab_typeid:0,
    list_active:0
  },
  onLoad(options){
    this.setData({
      id:options.id,
      pd:options.pd
    })
    this.getDetail()
  },
  getDetail(){
    api.userGetAddressDetail(this.data.id).then(res=>{
      this.setData({
        detail:res.data.data,
        name:res.data.data.name,
        mobile:res.data.data.mobile,
        address:res.data.data.address,
        full_region:res.data.data.full_region,
        is_default:res.data.data.is_default        
      })
    })
  },
  handleTap(){  //显示地址
    this.setData({
      show:true,
      tab_typeid:0
    })
    this.getAddressList(1)
  },
  getAddressList(id){  //获取地址方法
    api.userGetAddressList(id).then(res=>{
      this.setData({
        addressList:res.data.data
      })
    })   
  },
  hide(){  //隐藏显示地址
    this.setData({
      show:false
    })
  },
  tabtap(e){   //上方 tab切换
    let item = e.currentTarget.dataset.item
    this.setData({
      tab_typeid:item.type,
      list_active:item.params_id  //用于 展示列表样式
    })
    this.getAddressList(item.getid)
  },
  bindtapSwitch(e){   //地址 单击
    let item = e.currentTarget.dataset.item
    let tab_typeid = item.type
    let str = `tab[${tab_typeid}].getid`
    let params_id = `tab[${tab_typeid-1}].params_id`
    let params_name = `tab[${tab_typeid-1}].params_name`

    // 参数
    this.setData({
      list_active:item.id,
      [params_id]:item.id,
      [params_name]:item.name
    })

    this.computedAddress()  //调用计算 地址

    if(tab_typeid>2){
      return false
    }
    this.setData({
      tab_typeid,
      [str]:item.id,
    })
    this.getAddressList(item.id)
  },
  qdAddress(){  //确定地址
    let tabs = Object.assign(this.data.tab)
    let ids = []
    let pd = true
    tabs.forEach(v=>{
      if(!v.params_id) pd = false
      ids.push(v.params_id)
    })
    if(pd){
      this.setData({
        show:false
      })
    }else{
      wx.showToast({
        title: '请选择三级地址',
        icon: 'none',
      });
    }
  },
  computedAddress(){   //计算地址方法
    let str = ''
    this.data.tab.map((v,k)=>{
      if(k<=this.data.tab_typeid){
        if(v.params_name) str+=v.params_name + ' / '
      }
    })
    this.setData({
      full_region:str
    })
  },
  switchActive(){  //设为默认
    this.setData({
      is_default:!this.data.is_default
    })
  },
  call(){  //取消
    wx.navigateBack();
  },
  save(){  //保存
    let obj = {
      address:this.data.address,
      province_id:this.data.tab[0].params_id,
      city_id:this.data.tab[1].params_id,
      district_id:this.data.tab[2].params_id,
      is_default:this.data.is_default,
      mobile:this.data.mobile,
      name:this.data.name,
    }
    if(this.data.pd) obj.id = this.data.id
    api.userSetAddressSave(obj).then(res=>{
      wx.navigateBack()
    })
  }
})