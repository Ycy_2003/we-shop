import api from '../../../methods/api.js'
Page({
  data:{
    list:[]
  },
  onLoad(){
    this.getList()
  },
  async getList(){
    let res = await api.userFootprint()
    let str = `list[${this.data.list.length}]`
    this.setData({
      [str]:res.data.data.data
    })
    console.log(this.data.list);
  }
})