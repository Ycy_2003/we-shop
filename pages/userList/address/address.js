import api from '../../../methods/api.js'
Page({
  data: {
    list:[]
  },
  onShow(){
    this.getList()
  },
  onLoad(options){
    this.setData({
      pd:options.pd
    })
  },
  async getList(){
    let res = await api.userGetAddress()
    this.setData({
      list:res.data.data 
    })
  },
  fun(e){   //删除地址
    let item = e.detail.item
    let this_ = this
    wx.showModal({
      title:'确认删除',
      content:'是否删除当前地址',
      success (res) {
        if (res.confirm) {
          api.userDelAddress(item.id).then(res=>{
            this_.getList()
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  detail(e){
    let item = e.detail.item
    if(this.data.pd){   //判断从 下单页来时的事件
      console.log('sss');
      wx.setStorage({
        key:'address',
        data:item.id,
        success(){  //设置完返回
          wx.navigateBack();
        }
      })
      return false
    }
    wx.navigateTo({
      url: '/pages/userList/addressDetail/addressDetail?id='+item.id+'&pd='+true
    })
  },
  addNew(){
    wx.navigateTo({
      url:'/pages/userList/addressDetail/addressDetail?pd='+false
    })
  }
}) 