// pages/userList/collect/collect.js
import api from '../../../methods/api.js'
Page({
  data:{
    list:[]
  },
  onLoad(){
    this.getList()
  },
  async getList(){
    let res = await api.userCollect()
    let str = `list[${this.data.list.length}]`
    this.setData({
      [str]:res.data.data.data
    })
  }
})