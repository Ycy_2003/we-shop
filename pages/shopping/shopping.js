// pages/shopping/shopping.js
import api from '../../methods/api.js'
Page({
  data:{
    isXia:false,   //是否开启下拉刷新
    list:[],    //请求的列表数据
    allRadio:false,  //正常全选中 控制
    isSet:false,   //是否开始其 编辑
    setNum:0,   //编辑时 选中的件数
    setRadios:[],  //用于 编辑设置 以便于互相影响
    setQuanChecked:false  //编辑下的全选 控制
  },
  scrollXia(){
    setTimeout(()=>{
      this.setData({isXia:false})
    },900)
  },
  onShow(){
    this.getList()
  },
  async getList(){
    let res = await api.getShoppingList()
    let radios = res.data.data.cartList
    let arr = []   //用于 编辑的数组
    radios.forEach((v,k)=>{   //给用于编辑的数组 压入内容
      arr.push({product_id:v.product_id,id:v.id,pd:0,number:v.number})
    })
    this.setData({
      list:res.data.data,
      setRadios:arr
    })
    let a = 0
    res.data.data.cartList.map(v=>{
      if(v.checked) a++
    })
    if(a == res.data.data.cartList.length){
      this.setData({allRadio:true})
    }else{
      this.setData({allRadio:false})
    }
  },
  radioTap(e){
    let item = e.currentTarget.dataset.item
    let radio = 0 
    let id = item.product_id
    if(item.checked) radio = 0
    else radio =1
    api.setShopingRadio(radio,id).then(res=>{
      this.getList()
    })
  },
  shoppingSet(){  //开始编辑
    this.setData({
      isSet:!this.data.isSet
    })
    let arr = this.data.setRadios
    arr.forEach(v=>{
      v.pd = 0
    })
    this.setData({
      setQuanChecked:false,
      setRadios:arr
    })
  },
  radioDel(e){  //删除radip
    let item = e.currentTarget.dataset.item
    let key = 0
    let setRadios = this.data.setRadios   
    setRadios.map((v,k)=>{      //计算 单击的下标
      if(item.id == v.id) key = k
    })

    let setRadio = `setRadios[${key}].pd`  //用于更改
    let pd = 0
    if(item.pd == 0) pd = 1
    else pd = 0
    this.setData({[setRadio]:pd})
      
    let count = 0    //计算删除选中的个数
    let js = 0;var jspd = false
    this.data.setRadios.map((v)=>{  //计算需要删除的 id 
      if(v.pd == 1){
          count += v.number
          js+=1
      }
    })
    if(js == this.data.setRadios.length) jspd = true
    else jspd = false
    this.setData({
      setNum:count,
      setQuanChecked:jspd
    })
  },
  delAll(){   //删除所选
    let strArr = []   //用于 计算 删除传递参数 
    this.data.setRadios.map(v=>{
      if(v.pd == 1)  strArr.push(v.product_id)
    })
    let setArr = [...new Set(strArr)]  //删除的参数 处理为字符串
    let setStrs = setArr.toString()
    if(!setStrs.length) return false  //判断是否选中要删除的没有则 终止
    api.delShopping(setStrs).then(res=>{
      this.getList()
      this.setData({
        setQuanChecked:false
      })
      if(res.data.data.cartList.length ==0){
        this.setData({
          isSet:false
        })
      }
    })
  },
  QX(){  //全选
    this.setData({
      allRadio:!this.data.allRadio
    })
    let arrs = []
    this.data.list.cartList.map(v=>{
        arrs.push(v.product_id)
    })
    let a = arrs.toString()
    let isChecked = this.data.allRadio ==1 ? 1 : 0
    api.setShopingRadio(isChecked,a).then(res=>{
      this.getList() 
    })
  },
  updateJian(e){  //减
    this.Update(e,true)
  },
  updateJia(e){  //加
    this.Update(e,false)
  },
  setQuan(){   //全选
    let pd = !this.data.setQuanChecked
    let arr = this.data.setRadios
    arr.forEach(v=>{
      if(pd) v.pd =1
      else v.pd = 0
    })    
    this.setData({
      setRadios:arr,
      setQuanChecked:pd
    })
  },
  Update(e,switchPD){   //加减方法
    let item = e.currentTarget.dataset.item
    var num = item.number
    let allshop = this.data.list.cartList;
    let key = 0
    allshop.map((v,k)=>{
      if(v.id == item.id) key = k
    })
    if(switchPD) num--
    else num++
    if(num<=0) return false
    let str = `list.cartList[${key}].number`
    this.setData({
      [str]:num
    })
    let obj = {productId: item.product_id, goodsId: item.goods_id, number: num, id: item.id}
    api.ShoppingUpdate(obj)
  },

  PlaceTheOrder(){  //下单
    if(!wx.getStorageSync('user')){
      wx.showToast({
        title: '请先登录',
        icon: 'none',
      });
      return false
    }
    wx.navigateTo({
      url: '/pages/shoppingPTO/shoppingPTO'
    })
  },

  tapmove(){   //列表单击事件
    this.setData({
      movei:null
    })
  },
  touchStart(e){   //手指按下事件
    this.setData({
      startX: e.changedTouches[0].pageX  //获取手指按下的x轴位置
    })
  },
  touchEnd(e){
    if(this.data.isSet) return false //编辑模式下 终止
    let endX = e.changedTouches[0].pageX   //获取手指抬起的x轴位置
    let i = e.currentTarget.dataset.index
    if(endX-this.data.startX<=-30){
      this.setData({
        movei:i
      })      
    }else{
      this.setData({ 
        movei:null
      })  
    }
  },
  delZhan(e){  //左滑按钮删除
    let item_product_id = e.currentTarget.dataset.item.product_id+''
    api.delShopping(item_product_id).then(res=>{
      console.log(res);
      this.getList()
    })
  }
})