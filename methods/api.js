import ajax from './ajax.js'
function token(){
  return wx.getStorageSync('user') ?  wx.getStorageSync('user').token :''
}
export default {
  async getHome(){
    return await ajax('index/index','get')
  },
  async getCategory(id){  //小分类
    return await ajax('goods/category','get',{id},token())
  },
  async getCategoryList(id,page,size){  //小分类列表
    return await ajax('goods/list','get',{categoryId:id,page,size},token())
  },
  
  async getFeture(page,size){   //专题
    return await ajax('topic/list','get',{page,size},token())
  },
  getFetures:{
    async Detail(id){    //专题详细
      return await ajax('topic/detail','get',{id},token())
    },
    async related(id){
      return await ajax('topic/related','get',{id},token())
    },
    async list(id,tid,size){
      return await ajax('comment/list','get',{valueId:id,typeId:tid,size},token())
    },
    async PingList(valueId,typeId,size,page){
      return await ajax('comment/list','get',{valueId,typeId,size,page},token())
    },
    async addComment(obj){
      return await ajax('comment/post','post',obj,token())
    }
  },
  async getClass(){   //分类
    return await ajax('catalog/index','get',{})
  },
  async getRightClass(id){ //catalog
    return await ajax('catalog/current','get',{id})
  },


  // 搜索
  async getLsList(){
    return await ajax('search/index','get',{})
  },
  async getLSli(keyword){
    return await ajax('search/helper','get',{keyword})
  },
  async getSou(keyword,page,size,sort,order){    //搜索后列表
    return await ajax('goods/list','get',{keyword,page,size,sort,order,categoryId:'0'})
  },
  async delLSList(){
    return await ajax('search/clearhistory','post',{},token())
  },
  // 搜索结束

  // 品牌
  async getBrand(id){
    return await ajax('brand/detail','get',{id},token())
  },
  async getBrandLis(brandId,page,size){
    return await ajax('goods/list','get',{brandId,page,size},token())
  },

  // 列表详情
  async getDetail(id){ 
    return await ajax('goods/detail','get',{id},token())
  },
  async getDetailComments(valueId,page,showType){  //详情列表 评论
    return await ajax('comment/list','get',{valueId,typeId:0,size:10,page,showType},token())
  },
  async getDetailCommentNum(valueId){ //详情列表 评论数量
    return await ajax('comment/count','get',{valueId,typeId:0},token())
  },
  async getDetailGoods(id){  //详情列表 推荐
    return await ajax('goods/related','get',{id},token())
  },
  async addShopping(goodsId,number,productId){  //加入购物车
    return await ajax('cart/add','post',{goodsId,number,productId},token())
  },
  async shoppingNum(){
    return await ajax('cart/goodscount','get',{},token())
  },
  async Collect(valueId){   //收藏
    return await ajax('collect/addordelete','post',{typeId:0,valueId},token())
  },

  async getShoppingList(){   //购物车列表
    return await ajax('cart/index','get',{},token())
  },
  async setShopingRadio(isChecked,productIds){   //编辑
    return await ajax('cart/checked','post',{isChecked,productIds},token())
  },
  async delShopping(productIds){   //删除
    return await ajax('cart/delete','post',{productIds},token())
  },
  async ShoppingUpdate(obj){  //更改数数量
    return await ajax('cart/update','post',obj,token())
  },

  async login(code,userInfo){  //登录
    return await ajax('auth/loginByWeixin','post',{code,userInfo})
  },

  async userCollect(){   //我的收藏
    return await ajax('collect/list','get',{typeId:0},token())
  },
  async userFootprint(){  //足迹
    return await ajax('footprint/list','get',{},token())
  },
  async userGetAddress(){  //获取地址列表
    return await ajax('address/list','get',{},token())
  },
  async userDelAddress(id){  //删除地址
    return await ajax('address/delete','post',{id},token())
  },
  async userGetAddressDetail(id){  
    return await ajax('address/detail',"get",{id},token())
  },
  async userGetAddressList(parentId){   //获取 地址列表
    return await ajax('region/list','get',{parentId},token())
  },
  async userSetAddressSave(obj){  //保存 收获地址
    return await ajax('address/save','post',obj,token())
  },

  async PTO(addressId){  //下单信息
    return await ajax('cart/checkout','get',{addressId,couponId:0},token())
  },
  async PTOafterAddress(addressId){  //下单信息
    return await ajax('order/submit','post',{addressId,couponId:0},token())
  },
  async PTOafterOrder(orderId){  //下单
    return await ajax('pay/prepay','get',{orderId},token())
  }
}