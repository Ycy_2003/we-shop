const URL = 'http://192.168.1.3:8360/api/';
export default function(url,method='get',data,token){ 
  return new Promise((success,error)=>{
    wx.request({
      url:URL+url,   
      method, 
      data,
      header: {
        'Content-Type': 'application/json',
        'X-Nideshop-Token': token
      },
      success(msg){
        if(msg.statusCode>=200 && msg.statusCode<=300 || msg.statusCode==304 ){
          success(msg)
        }
      },
      error(err){
        error(err)
      }
    })
  })
}